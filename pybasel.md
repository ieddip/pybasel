% Intelligent Environment Documentation Through Distributed Image Processing in Wireless Sensor Networks
% Eric Matti at PyBasel (2016.05.10)
% IVGI, FHNW Research Project with avisec AG and funded by Forschungsfond AG


# Agenda

- Project Goals

- Dataflow Programming

- Implemented Embedded Event Processing (EEP) Library and Components

- Overall System

# Project Goals and Vision

- Development of a software framework for the distributed control of
  sensors and processing of data in sensor networks.

- Development of components for the acquisition, transmission,
  processing, analysis and management of webcam-images and other data
  in sensor networks.

- Development of components for smart user interfaces, which allow to
  control various processing components and to interact, explore and
  analyze collected data.

------

![](./resources/projectvisionnet.png)

------

![](./resources/projectvisionui.png)

# Dataflow Programming | Definition

_"A programming paradigm in which computation is modeled as a
__directed graph [...]__, the nodes of which are either __data
sources__ (producers of data), __data sinks__ (consumers), or
__"processing elements"__ which compute some function; and the
__arcs__ of which represent dataflow between nodes." ([Cunningham &
Cunningham, Inc, 2015][])_

# Dataflow Programming | (Some Random) Examples

- Spreadsheets

- Unix pipes

- Messaging in the actor model (Erlang, Akka, ...)

- Flow-based programming (noflojs, ...)

------

![](./resources/noflojs1.png)

------

![](./resources/noflojs2.png)

------

![](./resources/nodered.png)

# Dataflow Programming | Further Reading

![](./resources/books.jpg)

# Dataflow Programming | Further Reading and Links

- Flow-based programming by J Paul Morrison:
  http://jpaulmorrison.com/fbp/

- FBP Wiki by J Paul Morrison:
  http://www.jpaulmorrison.com/cgi-bin/wiki.pl

- NoFlo documentation: http://noflojs.org/documentation/

- Wikipedia: https://en.wikipedia.org/wiki/Flow-based_programming

- Apache Storm: https://storm.apache.org/

- Robot Operating System (ROS): http://www.ros.org/

# Dataflow Programming (and others) | Python Tools

- Pipeless: https://andychase.me/pipeless/

- PyF: http://pyfproject.org/

- Pypes: http://pypes.sourceforge.net/index.html

- Kamaelia: http://www.kamaelia.org/Home.html

- Ruffus: http://www.ruffus.org.uk/

- Graffiti: https://github.com/SegFaultAX/graffiti

- Lusmu: https://github.com/akaihola/lusmu

- Luigi: https://github.com/spotify/luigi

# Implemented Embedded Event Processing (EEP) Framework | First Prototype

[![](./resources/firstprototype.png)](./resources/firstprototype.mov)

------

![](./resources/firstprototypearchitecture.png)

# Implemented Embedded Event Processing (EEP) Library

	+-----------------------+
	|                       |
	|     graphite-cli      |
	|                       |
	+-----------------------+
	|           |           |
	| graphite  | gatherer  |
	|           |           |
	+-----------------------+
	|                       | -> with main logic in own libraries
	|      Components       |
	|                       |
	+-----------------------+

# Implemented Embedded Event Processing (EEP) Library | Starter Recipe

__Ingredients:__

- 1 x Event Emitter / Event Loop

- 4 x Helper functions

- n x 'Source', 'transformer' or 'sink' functions

------

```python
# - a minimal EEP library

# helper to create a callback function
def callback(eventemitter, topics):
	def f(payload):
		if issequential(topics):
			for topic in topics:
				eventemitter.notify(topic, payload)
		else:
			eventemitter.notify(topics, payload)
	return f

# wire up a 'tranformer' function
def transformer(eventemitter, topic, fnc, topics):
	def f(payload):
		callback(eventemitter, topics)(fnc(payload))
	eventemitter.add_callback(topic, f)

# wire up a 'sink' function
def sink(eventemitter, topic, fnc):
	eventemitter.add_callback(topic, fnc)

# wire up a 'source' function
def source(eventemitter, fnc, topics):
	fnc(callback(eventemitter, topics))
```

------

```python
# - a very simplified first example using our minimal EEP library

# definition of our functions / components

def printer(payload):
	print(payload)

def adder(payload):
	return payload + 1

def number(f):
	f(1)

eventemitter = EventEmitter()
sink(eventemitter, 'printer', printer)
transformer(eventemitter, 'adder', adder, 'printer')
source(eventemitter, number, 'adder')  # starts processing
```

# Implemented Embedded Event Processing (EEP) Library | Main Recipe

__Ingredients:__

- 1 x Event Emitter / Event Loop

- 1 x Representation of the processing tree and logic to build it up

- 1 x Runtime / logic to wire everything up, start and stop the processing

- n x 'Source', 'transformer' or 'sink' components

------

```clojure
(ns user
  (:require [graphite.runtime :as runtime]
			[graphite.graph :as graph]))

;; define 'source', 'transformer' and 'sink' components
(defn number [{:keys [n]}]
  (let [exec (fn [{:keys [handler]}] (handler n))]
	{:exec exec
	 :type :source}))

(defn adder [{:keys [n]}]
  (let [exec (fn [p] (+ p n))]
	{:exec exec
	 :type :transformer}))

(defn printer []
  (let [exec (fn [p] (println p))]
	{:exec exec
	:type :sink}))

;; define the processing graph
(def graph (graph/defgraph
			 [[:number number {:n 1}] -> [:adder adder {:n 2}]
			 [:adder] -> [:printer printer]]))

;; create runtime and start and stop it
(def r (runtime/create graph))
(runtime/start r)
(runtime/stop r)
```

# Implemented Embedded Event Processing (EEP) Library | Pros and Cons

__PROS:__

- Simple processing model,

- Very easy to implemented,

- Depending on the configuration of the event emitter /
  loop processing components __may__ run concurrently or sequently
  and different routing strategies __may__ be used (e.g. first, broadcast,
  round-robin).

------

__CONS:__

- Only push oriented,

- Payload oriented, only one input port per component.

__TIPS__:

- Schema validation, structural typing or using a typed language helps
  to ensure that only components that match up on the payload are
  wired together.

# Implemented Components (Just a Selection)

__IO:__

```clojure
[:x mqtt-publisher {:path "tcp://127.0.0.1:1883" :topic "camera1"}]

[:x mqtt-subscriber {:path "tcp://127.0.0.1:1883" :topics {"camera1" 0}}]

[:x couch-putter {:endpoint "http://127.0.0.1:5984/camera1"}]

[:x blob-store-putter {:endpoint "<url>"
					   :table "camera1"}]
					   
...
```

# CAMERA STUFF

```clojure
[:x image-trigger {:interval 30000}]

[:x scheduler {:schedule 
                {:weekend [{:start "07:00" :end "16:00" 
                           :actions [{:ival 60 :payload "every minute"}]}]}}]

[:image-capturer image-capturer] ;; using libgphoto2

...
```

# IMAGE PROCESSING

```clojure
[:x image-resizer {:w 400 :h 200}]

[:x image-cropper {:x 100 :y 100 :w 50 :h 50}]

[:x image-rotator {:d 45.0}]

[:x image-stabilizer ...]

...
```

------

```clojure
[:x image-perspective-transformer {:x0 0 :y0 0 
                                   :x1 1000 :y1 0 
								   :x2 800 :y2 667 
								   :x3 200 :y3 667}]
```

![](./resources/perspective.gif)


------

```clojure
[:x lv-calculator {:w 400 :h 200 :filter-type "filter-90"}]
[:x mad-classifier {:n 20 :threshold 3.2}]
```

![](./resources/blurry1.jpg)

------

![](./resources/blurry2.jpg)

------

```clojure
[:x ieddip.image.core/background-imager {:n 5}]
```

![](./resources/backgrounds.gif)

# Overall System | Hardware

![](./resources/camera1.jpg)

# Overall System | Hardware

![](./resources/camera2.png)

# Overall System | Hardware

![](./resources/camera3.png)

------

__Overall System | Software__

![](./resources/architecture.png)

------

![](./resources/ui1.png)

------

![](./resources/ui2.png)

------

__Overall System | Software__

Additional components:

- Graphite (for EEP)

- Ansible (for DevOps / Deployment)

- REST-Webservice (for storing time series data)

- ELK-Stack (for central logging)

- MQTT (for communication)

- VPN (for accessing cameras)

# Misc

![](./resources/3.png)

--------

![](./resources/4.png)

--------

![](./resources/5.png)

--------

![](./resources/6.png)

--------

![](./resources/7.png)

[Cunningham & Cunningham, Inc, 2015]: http://c2.com/cgi/wiki?DataflowProgramming
