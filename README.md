# pybasel slides (2016.05.10)

A DZSlides presentation that can be built with
[pandoc](http://pandoc.org/),
[GNU Make](https://www.gnu.org/software/make/) and
[imagemagick](http://www.imagemagick.org).

# How-to

- Just open html file in your browser or if you want to re-build the
  slides run `make`.

## Dependencies

- [pandoc](http://pandoc.org/): http://pandoc.org/

- [GNU Make](https://www.gnu.org/software/make/): https://www.gnu.org/software/make/
